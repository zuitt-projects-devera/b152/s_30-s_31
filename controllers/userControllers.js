const User = require('../models/User');

//add user controller
module.exports.createUserController = (req,res) => {

	User.findOne({username: req.body.username})
	.then(result => {
		//console.log(result)
		if(result !== null && result.username === req.body.username) {
			return res.send("Duplicate User Found");
		}
		else {
			let newUser = new User({
			username: req.body.username,
			password: req.body.password
	});

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(err));
};

//get all users controller
module.exports.getAllUsersController = (req,res) => {
	 User.find({})
	 .then(result => res.send(result))
	 .catch(error => res.send(error));
}

//get single user controller
module.exports.getSingleUserController = (req, res) => {
	 User.findById(req.params.id)
	 .then(result => res.send(result))
	 .catch(error => res.send(error));
}

//update username controller
module.exports.updateUsernameController = (req, res) => {
		let newUsername = {
			username: req.body.username
		}
		User.findByIdAndUpdate(req.params.id, newUsername, {new: true})
		.then(updatedUser => res.send(updatedUser))
		.catch(error => res.send(error));
}