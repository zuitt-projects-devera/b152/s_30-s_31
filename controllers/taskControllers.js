/*import the task model in the controllers. So that our controllers or controller functions may have access to
our Task model. */
const Task = require('../models/Task');

/*module.exports will allow us to add the controllers as methods for our module. This controller 
module can be imported in other files. Modules in JS are considered objects.*/

//add task controller
module.exports.createTaskController = (req, res) => {
//console.log(req.body);

	//Task model is a constructor.
	//newTask that will be created from our Task model will have additional methods to be used in our app
    let newTask = new Task({
    	name: req.body.name,
    	status: req.body.status,
    	description: req.body.description
});
    /*.save() is a method from an object created by a model.
    Allows us to save the document into the collection.
    save() can have anonymous function or we can have what we call a then chain.
    
    The anonymous function in the save() method is used to handle the error or the proper result/response from mongodb
   	
   	.then and catch chain:

   	.then is used to handle the proper result/returned value of a function. If the function preprly returns a value,
   	we can run a separate function to handle it.
   	
   	.catch() is used to handle/catch the error from the use of a function. So that if theres an error, we can 
   	properly handle it separate from the result.*/

    newTask.save()
    .then(result => res.send(result))
    .catch(error => res.send(error));
}

//get all tasks
module.exports.getAllTaskController = (req,res) => {

	/*To be able to query with a collection, we use the Task model and its find() function.
	This find() function is similar to mongoDB's own find(). This will allow us to cnnect to our 
	collection and retrieve all documents that matches our criteria.
	Similar to db.tasks.find({})*/
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

//get single task
module.exports.getSingleTaskController = (req, res) => {
		/*let id = parseInt(req.params.id);*/
		//console.log(req.params);
		//console.log(req.params);
		/*Task.find({"_id": id})
		.then(result => res.send(result))
		.catch(error => res.send(error));*/
		//console.log(req.params.id);
		Task.findById(req.params.id)
		.then(result => res.send(result))
		.catch(error => res.send(error));
}

//update status controller
module.exports.updateTaskStatusController = (req, res) => {
		console.log(req.params.id);
		console.log(req.body);
		/*Model.findByIdAndUpdate() = db.collection.udpateOne({_id: "id"}, {$set{property: newValue}})*/
		//Syntax: Model.findByIdAndUpdate(id, {updates}, {new:true})

		/*The third argument: {new:true}, allows us to return the updated version of the document we were 
		updating. By default, without this argument, findByIdAndUpdate*/
		let updates = {
			status: req.body.status
		}
		Task.findByIdAndUpdate(req.params.id, updates, {new: true})
		.then(updatedTask => res.send(updatedTask))
		.catch(error => res.send(error));
}