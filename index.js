//npm init - creating package.json
//npm install express - install the express package
//npm install mongoose - install mongoose
//npm install nodemon - install live server (no need to turn on/off the server when changes are made.)


/* 

	change package.json ()
	npm start - production
   npm run dev - development */


const express = require('express');
/* Mongoose is ap ackage which used an ODM, or object document mapper. It 
allows us to translate our JS objects into database documents for MongoDB. IT allows us connection and
easier manipulation of our documents in mongoDB.*/
const mongoose = require('mongoose');
const app = express();
const port = 4000;
const Task = require('./models/Task');
const User = require('./models/User');



//mongoose connection
/* mongoose.connect is the method to connect your apu to your mongodb via the use of mongoose. It has 
2 arguments. First, is the connection string to connect our api to our mongodb atlas. Second, is an object 
used to add information between mongoose and mongodb.

replace/change <password> in the connection string to your db password

replace/change myFirstDatabase to task152

MongoDB upon connection and creating our first documents will create the task152 database for us

*/
mongoose.connect("mongodb+srv://user1-blue:admin123@cluster0.p9on8.mongodb.net/task152?retryWrites=true&w=majority",
	{
		useNewUrlParser: true, useUnifiedTopology:true
	});
//We wil create notifications if the connection to the database succeed or failed.
let db = mongoose.connection;
//We add this so that when DB has an error, we will show the connection error
//in both terminal and in the browser for our client.
db.on('error', console.error.bind(console, "connection error."));
//once the connection is open and successful, we wull output a message in the terminal
db.once('open', ()=>console.log('Connected to MongoDB.'));

//Middleware - A middleware, in expressjs context, are methods, functions that acts and adds features to our
//application
//express.json() - handle the body of request. It handles the JSON data from our client
app.use(express.json());

const taskRoutes = require('./routes/taskRoutes');
//Our server will use middleware to group all task routes under /tasks.
//Meaning to say, all the endpoints in taskRoutes file will start with /tasks
app.use('/tasks', taskRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);


app.listen(port,()=>console.log(`Server is running at port ${port}`));