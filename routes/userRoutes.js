const express = require('express');
//Router() is a method from express that allows us the access to our HTTP method routes.
//Router() will act as a middleware and our routing system.
const router = express.Router();
const User = require('../models/User');

const userControllers = require('../controllers/userControllers');
//add user route
//http://localhost:4000/users
router.post('/', userControllers.createUserController);

//gettAllUser route
//http://localhost:4000/users
router.get('/', userControllers.getAllUsersController);

//getSingleUser route
//http://localhost:4000/users/getSingleUser/61ef660f1feb46b967675681
router.get('/getSingleUser/:id', userControllers.getSingleUserController);

//update username route
//http://localhost:4000/users/updateUsername/61ef660f1feb46b967675681
router.get('/updateUsername/:id', userControllers.updateUsernameController);


/*router holds all our routes, it will be what we will export or import in another file.*/
module.exports = router;